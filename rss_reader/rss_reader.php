<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
      $rssFeed = array(
        "https://www.rivm.nl/nieuws/rss.xml"
      );

      $feedEntries = array();
      foreach ($rssFeed as $feed) {
        $xml = simplexml_load_file($feed);
        $feedEntries = array_merge($feedEntries, $xml->xpath("//item"));
      }

      usort($feedEntries, function($feed1, $feed2) {
        return strtotime($feed2->pubDate) - strtotime($feed1->pubDate);
      });
     ?>

     <ul>
      <?php foreach ($feedEntries as $entry): ?>
        <li> <a href="<?php echo $entry->link ?>"><?php echo $entry->title ?></a> (<?php echo parse_url($entry->link)['host'] ?>)
          <p><?= strftime('%m/%d/%Y %I:%M %p', strtotime($entry->pubDate)) ?></p>
          <p><?= $entry->description ?></p></li>
        </li>
      <?php endforeach ?>
    </ul>


  </body>
</html>
