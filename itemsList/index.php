<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
      // Include the dhb and item class
      include_once 'includes/dbh.inc.php';
      include_once 'includes/item.inc.php';
      $object = new Item;
      // echo $object->getItemByName("ta");

      foreach ($object->getAllItems() as $row) {
        // Print name
        echo $row["name"]."<br />\n";
      }
     ?>
  </body>
</html>
