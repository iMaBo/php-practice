<?php

class Item extends Dbh
{

  // getAllItems function
  public function getAllItems() {
    // Create query to get all items inside table items
    $stmt = $this->connect()->query("SELECT * FROM `items`")->fetchAll();

    return $stmt;
  }

  // getItemByName which is preset in index.php
  public function getItemByName($name) {
    // Create query to get results like name in table items
    $stmt = $this->connect()->prepare("SELECT * FROM `items` WHERE name LIKE '%' ? '%'");
    // appoint parameter to prepared statement
    $stmt->execute([$name]);
    $attr = $stmt->fetchAll();

    return $stmt;
  }

  public function removeItemById($id) {
    $stmt = $this->connect()->prepare("DELETE FROM `items` WHERE id = ?");
    $stmt->execute([$id]);

    $stmt = null;
  }

  public function insertItem($name, $label, $weight, $type, $ammotype, $image, $unique, $useable, $shouldClose, $description, $combinable) {
    $stmt = $this->connect()->prepare("INSERT INTO `items` (name, label, weight, type, ammotype, unique, useable, shouldClose, description, combinable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->execute([$name, $label, $weight, $type, $ammotype, $image, $unique, $useable, $shouldClose, $description, $combinable]);

    $stmt = null;
  }

  public function updateItem() {

  }

}
