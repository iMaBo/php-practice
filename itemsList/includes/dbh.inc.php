<?php

class Dbh
{
  // Preset private variables
  private $host;
  private $username;
  private $password;
  private $dbname;
  private $charset;


  public function connect() {
    // appoint data to variables?
    $this->host = "localhost";
    $this->username = "root";
    $this->password = "";
    $this->dbname = "lotusitemdb";
    $this->charset = "utf8mb4";

    try {
      // Setup Data Source name for PDO
      $dsn = "mysql:host=".$this->host.";dbname=".$this->dbname.";charset=".$this->charset;
      // Create new PDO
      $pdo = new PDO($dsn, $this->username, $this->password);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $pdo;
    } catch (PDOException $e) {
      // Print error message
      echo "Connection failed: ".$e->getMessage();
    }
  }
}
