<?php

// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";

$todoName = $_POST['todo_name'] ?? ''; // Retrieve POST request data
$todoName = trim($todoName); // Remove whitespace

if ($todoName) {
  if (file_exists('todo.json')) { // Check if file exists
    $json = file_get_contents('todo.json'); // Get Json File
    $jsonArray = json_decode($json, true); // Decode Json to an array
  } else {
    $jsonArray = []; // create empty array for new file
  }
  $jsonArray[$todoName] = ['completed' => false]; // add todo jsonArray

  file_put_contents('todo.json', json_encode($jsonArray)); // Save new jsonArray to todo.json file
}

header('Location: todo.php'); // Redirect user back to index.php

?>
