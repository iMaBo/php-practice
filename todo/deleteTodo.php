<?php

  $json = file_get_contents('todo.json'); // Get Json File
  $jsonArray = json_decode($json, true); // Decode Json to an array

  $todoName = $_POST['todo_name'];

  unset($jsonArray[$todoName]);

  file_put_contents('todo.json', json_encode($jsonArray, JSON_PRETTY_PRINT));

  header('Location: todo.php');
 ?>
